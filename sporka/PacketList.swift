//
//  PacketList.swift
//  sporka
//
//  Created by tom Hastik on 28/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

enum PacketList {

    case getTransparentAccounts
    case getAccountId(id: String)
    case getTransactions(id: String, page: Int, size: Int)

    var baseUrl: String {

        #if RELEASE
        return "https://www.csas.cz/webapi/api/v3/"
        #elseif DEBUG
        return "https://webapi.developers.erstegroup.com/api/csas/sandbox/v3/"
        #endif
    }

    var url: String {

        switch self {

            case .getTransparentAccounts: return "\(baseUrl)transparentAccounts/?page=0&size=25&filter=example"

            case .getAccountId(let id): return "\(baseUrl)transparentAccounts/\(id)"

            case .getTransactions(let id, let page, let size): return "\(baseUrl)transparentAccounts/\(id)/transactions/?page=\(page)&size=\(size)"
        }
    }
}
