//
//  ViewController.swift
//  sporka
//
//  Created by tom Hastik on 28/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {

    // MARK: - Properties

    lazy var accountsDataSource: AccountDataSource = {
        return AccountDataSource(identifier: AccountCell.reuseIdentifier)
    }()

    lazy var accountsTableView: UITableView = {

        let padding = UINavigationBar.navbarSize + UINavigationBar.navbarPadding
        let rect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - padding)

        let accountsTableView = UITableView(frame: rect)

        accountsTableView.register(AccountCell.self)
        accountsTableView.rowHeight = 165
        accountsTableView.delegate = self
        accountsTableView.dataSource = self.accountsDataSource
        accountsTableView.tableFooterView = UIView(frame: .zero)

        if #available(iOS 9.0, *) {
            accountsTableView.cellLayoutMarginsFollowReadableWidth = false
        }

        return accountsTableView
    }()

    lazy var progressIndicator: ActivityView = {

        let indicator = ActivityView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        indicator.center = self.view.center

        return indicator

    }()

    //  MARK: - UIView

    override func viewDidLoad() {

        super.viewDidLoad()

        self.view.add(self.accountsTableView, self.progressIndicator)

        SporkaAPI.getTransparentAccounts { result in

            switch result {

                case .success(let response):

                    self.accountsDataSource.originalData = response

                    DispatchQueue.main.async {

                        self.progressIndicator.removeFromSuperview()
                        self.accountsTableView.reloadData()
                    }

                case .failure(let error): print(">>> \(error)")
            }
        }
    }

    // MARK: - Auxiliary

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//  MARK: - UITableView

extension ViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let currentItem = accountsDataSource.originalData[indexPath.row]
        let detailController = DetailViewController(currentItem)

        self.navigationController?.pushViewController(detailController, animated: true)

    }
}
