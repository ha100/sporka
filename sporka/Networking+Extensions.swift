//
//  Networking+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

extension Networking {

    static func sendPacket(packet: PacketList, completion: @escaping (Result<JSON, SporkaError>) -> ()) {

        let result = self.generateRequest(forPacket: packet)

        switch result {

            case .success(let request):

                URLSession.shared.dataTask(with: request) { (data, response, error) in

                    guard
                        let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                        let responseData = data, error == nil else {

                            if let code = (response as? HTTPURLResponse)?.statusCode {

                                switch code {

                                    case 412: completion( .failure( .WebApiKeyIncorrect ) )

                                    default: completion( .failure( .NetworkError( message: ">>> \( code ) >>> \(String(describing:  error ))") ) )
                                }
                            }

                            return
                    }

                    let decodedData = responseData.decodeJSON()

                    switch decodedData {

                        case .success(let dict):

                            print(">>> \(dict)")
                            completion( .success( dict ) )

                        case .failure(let error): completion( .failure( .JsonCreationError( message: "\( error )" ) ) )
                    }

                }.resume()

            case .failure(let error): completion( .failure( error ) )

        }
    }

    static func safelyRetrieveApiKey() -> Result<String, SporkaError> {

        let token = Bundle.main.path(forResource: "Sensitive", ofType: "plist")
            .map { NSDictionary(contentsOfFile: $0) as? Dictionary<String, String> }
            .flatMap { ($0?["WEB-API-key"]) }

        if token != nil {
            return .success( token! )
        } else {
            return .failure( .WebApiKeyMissing )
        }
    }

    static func generateRequest(forPacket: PacketList) -> Result<URLRequest, SporkaError> {

        guard let url = URL(string: forPacket.url) else { return .failure( .UrlCreationError(message: forPacket.url) ) }

        var request = URLRequest(url: url)

        request.httpMethod = "GET"

        let result = safelyRetrieveApiKey()

        switch result {

            case .success(let token):

                request.addValue(token, forHTTPHeaderField: "WEB-API-key")
                request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.addValue("cs-CZ", forHTTPHeaderField: "Accept-Language")
                request.addValue("gzip", forHTTPHeaderField: "Accept-Encoding")

                request.cachePolicy = .reloadIgnoringCacheData

                return .success( request )

            case .failure(let error): return .failure( error )
        }
    }
}
