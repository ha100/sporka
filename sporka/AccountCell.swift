//
//  AccountCell.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class AccountCell: UITableViewCell {

    lazy var dateFormatter: DateFormatter = {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YY HH:mm"

        return dateFormatter
    }()

    let sidePadding: CGFloat = 10
    let textSize: CGFloat = 12.0

    var accountName: String { return account?.name.map { $0 } ?? "N/A" }
    var accountNumber: String { return account?.accountNumber.map { $0 } ?? "N/A" }
    var accountBalance: String { return account?.balance.map { "\($0)" } ?? "N/A" }
    var accountCurrency: String { return account?.currency.map { $0 } ?? "N/A" }
    var accountIban: String { return account?.iban.map { $0 } ?? "N/A" }

    var actualizationDate: String { return dateFormatter.string(from: (account?.actualizationDate)!) }
    var publicationTo: String { return dateFormatter.string(from: (account?.publicationTo)!) }
    var transparencyTo: String { return dateFormatter.string(from: (account?.transparencyTo)!) }

    var account: Account?

    lazy var grayTextFontAttributes: AttributeDictionary = {

        return [ NSAttributedString.Key.font: UIFont(name: "Helvetica", size: self.textSize)!,
                 NSAttributedString.Key.foregroundColor: UIColor.gray]
    }()

    //  MARK: - Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override func draw(_ rect: CGRect) {

        let rectWidth = rect.size.width - CGFloat(2 * sidePadding)

        "ACCOUNT: \(accountName)".draw(in: CGRect(x: sidePadding, y: sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)

        "IBAN: \(accountIban)".draw(in: CGRect(x: sidePadding, y: 3 * sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)

        "ACCOUNT NUMBER: \(accountNumber)".draw(in: CGRect(x: sidePadding, y: 5 * sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)

        "BALANCE: \(accountBalance) \(accountCurrency)".draw(in: CGRect(x: sidePadding, y: 7 * sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)

        "PUBLISHED: \(publicationTo)".draw(in: CGRect(x: sidePadding, y: 10 * sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)

        "REFRESHED: \(actualizationDate)".draw(in: CGRect(x: sidePadding, y: 12 * sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)

        "TRANSPARENT: \(transparencyTo)".draw(in: CGRect(x: sidePadding, y: 14 * sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)
    }
}

extension AccountCell: TableCellCustomizable {

    func configureCellForObject<T>(item object: T) {

        guard let account = object as? Account else { print(">>> cell init with invalid object: \(object)"); return }

        self.account = account
        self.backgroundColor = .white
        self.selectionStyle = .none

        self.setNeedsDisplay()
    }
}
