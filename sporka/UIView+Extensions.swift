//
//  UIView+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 27/11/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import UIKit

extension UIView {

    func add(_ subviews: UIView...) {
        subviews.forEach(addSubview)
    }

    func add(_ subviews: [UIView]) {
        subviews.forEach(addSubview)
    }
}
