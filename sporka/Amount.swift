//
//  Amount.swift
//  sporka
//
//  Created by tom Hastik on 08/04/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct Amount {

    let currency: String?
    let precision: Int?
    let value: Int

    init(_ withDict: Dictionary<String, AnyObject>) {

        currency = withDict["currency"] as? String ?? ""
        precision = withDict["precision"] as? Int ?? -1
        value = withDict["value"] as? Int ?? 0
    }
}
