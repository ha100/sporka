//
//  Result.swift
//  sporka
//
//  Created by tom Hastik on 08/04/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

enum Result<T, E: Error> {

    case success(T)
    case failure(E)
}
