//
//  ArrayDataSource+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

extension ArrayDataSource: Collection {

    typealias Index = Int

    var startIndex: Index {
        return originalData.startIndex
    }

    var endIndex: Index {
        return originalData.endIndex
    }

    func index(before index: Index) -> Index {
        return index - 1
    }

    func index(after index: Index) -> Index {
        return index + 1
    }

    subscript(position: Index) -> T {
        return originalData[position]
    }

    func append(_ newElement: T) {
        originalData.append(newElement)
    }
}
