//
//  ActivityView.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class ActivityView: UIView {

    // MARK: - Properties

    var circleRadius: CGFloat { return self.frame.size.width / 2 }
    let duration = 2.0

    lazy var circlePathLayer: CAShapeLayer = {

        let circlePathLayer = CAShapeLayer()
        circlePathLayer.frame = self.bounds
        circlePathLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        circlePathLayer.lineWidth = 7
        circlePathLayer.fillColor = UIColor.clear.cgColor
        circlePathLayer.strokeColor = UIColor.sporkaBlue.cgColor

        return circlePathLayer
    }()

    lazy var circleLayer: CAShapeLayer = {

        let circleLayer = CAShapeLayer()
        circleLayer.frame = self.bounds
        circleLayer.lineWidth = 11
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = UIColor.gray.withAlphaComponent(0.5).cgColor

        return circleLayer
    }()

    lazy var groupAnimation: CAAnimationGroup = {

        let inAnimation = CAKeyframeAnimation()
        inAnimation.keyPath = "strokeEnd"
        inAnimation.duration = self.duration
        inAnimation.values = [0, 0.7, 1]

        let outAnimation = CAKeyframeAnimation()
        outAnimation.keyPath = "strokeStart"
        outAnimation.beginTime = self.duration * 0.5
        outAnimation.duration = self.duration * 0.5
        outAnimation.values = [0, 0.6, 1]

        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [inAnimation, outAnimation]
        groupAnimation.duration = self.duration
        groupAnimation.repeatCount = Float.infinity
        groupAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        groupAnimation.fillMode = .forwards
        groupAnimation.isRemovedOnCompletion = false

        return groupAnimation
    }()

    lazy var rotationAnimation: CABasicAnimation = {

        let rotationAnimation = CABasicAnimation()
        rotationAnimation.keyPath = "transform.rotation.z"
        rotationAnimation.fromValue = 0
        rotationAnimation.toValue = Double.pi * 2
        rotationAnimation.duration = self.duration
        rotationAnimation.repeatCount = Float.infinity
        rotationAnimation.isRemovedOnCompletion = false

        return rotationAnimation
    }()

    // MARK: - Init

    override init(frame: CGRect) {

        super.init(frame: frame)
        configure()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    func configure() {

        layer.addSublayer(circleLayer)
        layer.addSublayer(circlePathLayer)

        backgroundColor = .clear

        self.rotate()
    }

    func rotate() {

        self.circlePathLayer.add(self.rotationAnimation, forKey: nil)
        self.circlePathLayer.add(self.groupAnimation, forKey: nil)
    }

    override func layoutSubviews() {

        super.layoutSubviews()

        circleLayer.frame = bounds
        circleLayer.path = UIBezierPath(ovalIn: self.bounds.insetBy(dx: 10, dy: 10)).cgPath

        circlePathLayer.frame = bounds
        circlePathLayer.path = UIBezierPath(ovalIn: self.bounds.insetBy(dx: 10, dy: 10)).cgPath
        circlePathLayer.lineCap = .round
    }
}
