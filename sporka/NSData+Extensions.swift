//
//  NSData+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 08/04/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

extension Data {

    func decodeJSON() -> Result<JSON, SporkaError> {

        if case let json as JSON = try? JSONSerialization.jsonObject(with: self, options: []) {
            return .success( json )
        }

        return .failure( .JsonCreationError(message: "decode") )
    }
}
