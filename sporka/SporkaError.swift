//
//  SporkaError.swift
//  sporka
//
//  Created by tom Hastik on 28/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

enum SporkaError: Error {
    
    case JsonCreationError(message: String)
    case NetworkError(message: String)
    case RequestCreationError
    case UrlCreationError(message: String)
    case UnknownPacketFormat(message: String)

    case WebApiKeyIncorrect
    case WebApiKeyMissing

    case GetTransparentAccountsError(message: String)
    case GetTransparentAccountIdError(message: String)
    case GetTransactionsError(message: String)
    
    init(msg: String) {
        self = .NetworkError(message: msg)
    }
}
