//
//  DetailViewController.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class DetailViewController: UIViewController {

    let topPadding: CGFloat = 100

    var account: Account! {
        didSet {
            self.accountName.text = self.account.name
        }
    }

    lazy var accountName: UILabel = {

        let accountName = UILabel()

        accountName.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: self.topPadding)
        accountName.font = UIFont.systemFont(ofSize: 14)
        accountName.backgroundColor = UIColor(0x005395, alpha: 0.6)
        accountName.textColor = .black
        accountName.textAlignment = .center
        accountName.numberOfLines = 0

        return accountName
    }()

    lazy var transactionsDataSource: TransactionDataSource = {
        return TransactionDataSource(identifier: TransactionCell.reuseIdentifier)
    }()

    lazy var transactionsTableView: UITableView = {

        let padding = UINavigationBar.navbarSize + UINavigationBar.navbarPadding
        let rect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - padding)

        let transactionsTableView = UITableView(frame: rect)

        transactionsTableView.register(TransactionCell.self)
        transactionsTableView.rowHeight = 65
        transactionsTableView.delegate = self
        transactionsTableView.dataSource = self.transactionsDataSource
        transactionsTableView.tableFooterView = UIView(frame: CGRect.zero)

        if #available(iOS 9.0, *) {
            transactionsTableView.cellLayoutMarginsFollowReadableWidth = false
        }

        return transactionsTableView
    }()

    lazy var progressIndicator: ActivityView = {

        let indicator = ActivityView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        indicator.center = self.view.center

        return indicator

    }()

    //  MARK: - Init

    init(_ withAccount: Account) {

        super.init(nibName: nil, bundle: nil)

        defer {

            self.account = withAccount
            self.navigationItem.title = withAccount.accountNumber

            self.view.addSubview(self.progressIndicator)

            SporkaAPI.getTransactions(id: withAccount.accountNumber!, page: 0, size: 25) { result in

                switch result {

                    case .success(let response):

                        self.transactionsDataSource.originalData = response

                        DispatchQueue.main.async {

                            self.progressIndicator.removeFromSuperview()
                            self.transactionsTableView.reloadData()
                        }

                    case .failure(let error): print(">>> \(error)")
                }
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("")
    }

    //  MARK: - UIView

    override func viewDidLoad() {

        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.view.add(self.accountName, self.transactionsTableView)
    }

    // MARK: - Auxiliary

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//  MARK: - UITableView

extension DetailViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let currentItem = transactionsDataSource.originalData[indexPath.row]

        print(">>> \(currentItem)")
    }
}
