//
//  TransactionCell.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class TransactionCell: UITableViewCell {

    lazy var dateFormatter: DateFormatter = {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YY HH:mm"

        return dateFormatter
    }()

    let sidePadding: CGFloat = 10
    let textSize: CGFloat = 12.0

    var transactionSender: String { return transaction?.sender?.name.map { $0 } ?? "N/A" }
    var transactionReceiver: String { return transaction?.receiver?.iban.map { $0 } ?? "N/A" }

    var transaction: Transaction?

    lazy var grayTextFontAttributes: Dictionary<NSAttributedString.Key, NSObject> = {

        return [ NSAttributedString.Key.font: UIFont(name: "Helvetica", size: self.textSize)!,
                 NSAttributedString.Key.foregroundColor: UIColor.gray]
    }()

    //  MARK: - Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    override func draw(_ rect: CGRect) {

        let rectWidth = rect.size.width - CGFloat(2 * sidePadding)

        "SENDER: \(transactionSender)".draw(in: CGRect(x: sidePadding, y: sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)

        "RECEIVER: \(transactionReceiver)".draw(in: CGRect(x: sidePadding, y: 3 * sidePadding, width: rectWidth, height: textSize + sidePadding), withAttributes: grayTextFontAttributes)
    }
}

extension TransactionCell: TableCellCustomizable {

    func configureCellForObject<T>(item object: T) {

        guard let transaction = object as? Transaction else { print(">>> cell init with invalid object: \(object)"); return }

        self.transaction = transaction
        self.backgroundColor = .white
        self.selectionStyle = .none

        self.setNeedsDisplay()
    }
}
