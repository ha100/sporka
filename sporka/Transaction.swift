//
//  Transaction.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct Transaction {

    let amount: Amount?
    let dueDate: Date?
    let processingDate: Date?
    let receiver: Receiver?
    let sender: Sender?
    let type: Int?

    init(_ withDict: Dictionary<String, AnyObject>) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        amount = (withDict["amount"] as? Dictionary<String, AnyObject>).map { Amount($0) }
        dueDate = dateFormatter.date(from: withDict["dueDate"] as? String ?? "") ?? nil
        processingDate = dateFormatter.date(from: withDict["processingDate"] as? String ?? "") ?? nil
        receiver = (withDict["receiver"] as? NSDictionary).map { Receiver($0) }
        sender = (withDict["sender"] as? Dictionary<String, AnyObject>).map { Sender($0) }
        type = withDict["type"] as? Int ?? -1
    }
}
