//
//  Receiver.swift
//  sporka
//
//  Created by tom Hastik on 08/04/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct Receiver {

    let accountNumber: String?
    let bankCode: Int?
    let iban: String?

    init(_ withDict: NSDictionary) {

        accountNumber = withDict["accountNumber"] as? String ?? ""
        bankCode = withDict["bankCode"] as? Int ?? -1
        iban = withDict["iban"] as? String ?? ""
    }
}
