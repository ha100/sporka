//
//  UIColor+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 28/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

extension UIColor {

    static let sporkaBlue = UIColor(0x005395)
    static let sporkaLightGray = UIColor(0xe1e1e1)

    convenience init(_ hex: Int, alpha: CGFloat = 1) {
        
        let components = (
            
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        
        self.init(red: components.R, green: components.G, blue: components.B, alpha: alpha)
    }
}
