//
//  UITableViewCell+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

extension UITableViewCell: ReusableView {}
