//
//  Sender.swift
//  sporka
//
//  Created by tom Hastik on 08/04/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct Sender {

    let accountNumber: String?
    let bankCode: String?
    let constantSymbol: String?
    let iban: String?
    let name: String?
    let variableSymbol: Int?
    let specificSymbol: String?
    let specificSymbolParty: String?

    init(_ withDict: Dictionary<String, AnyObject>) {

        accountNumber = withDict["accountNumber"] as? String ?? ""
        bankCode = withDict["bankCode"] as? String ?? ""
        constantSymbol = withDict["constantSymbol"] as? String ?? ""
        iban = withDict["iban"] as? String ?? ""
        name = withDict["name"] as? String ?? ""
        variableSymbol = withDict["variableSymbol"] as? Int ?? -1
        specificSymbol = withDict["specificSymbol"] as? String ?? ""
        specificSymbolParty = withDict["specificSymbolParty"] as? String ?? ""
    }
}
