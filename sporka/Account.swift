//
//  Account.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct Account {

    let accountNumber: String?
    let actualizationDate: Date?
    let balance: String?
    let bankCode: String?
    let currency: String?
    let iban: String?
    let name: String?
    let publicationTo: Date?
    let transparencyFrom: Date?
    let transparencyTo: Date?

    init(_ withDict: Dictionary<String, AnyObject>) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        accountNumber = withDict["accountNumber"] as? String ?? ""
        actualizationDate = dateFormatter.date(from: withDict["actualizationDate"] as? String ?? "") ?? nil
        balance = withDict["balance"] as? String ?? ""
        bankCode = withDict["bankCode"] as? String ?? ""
        currency = withDict["currency"] as? String ?? ""
        iban = withDict["iban"] as? String ?? ""
        name = withDict["name"] as? String ?? ""
        publicationTo = dateFormatter.date(from: withDict["publicationTo"] as? String ?? "") ?? nil
        transparencyFrom = dateFormatter.date(from: withDict["transparencyFrom"] as? String ?? "") ?? nil
        transparencyTo = dateFormatter.date(from: withDict["transparencyTo"] as? String ?? "") ?? nil
    }
}
