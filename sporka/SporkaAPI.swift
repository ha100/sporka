//
//  SporkaAPI.swift
//  sporka
//
//  Created by tom Hastik on 28/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

final class SporkaAPI: Networking {

    static func getTransparentAccounts(completion: @escaping (Result<AccountArray, SporkaError>) -> ()) {

        sendPacket(packet: .getTransparentAccounts) { result in

            switch result {

                case .success(let response):

                    if let accounts = response["accounts"] as? Array<Dictionary<String, AnyObject>> {

                        let transformed = accounts.map { Account($0) }

                        completion( .success( transformed ) )
                    } else {
                        completion( .failure( .UnknownPacketFormat(message: "\(#function)") ) )
                    }

                case .failure(let error): completion( .failure( .GetTransparentAccountsError(message: "\(error)") ) )
            }
        }
    }

    static func getTransparentAccountId(id: String, completion: @escaping (Result<JSON, SporkaError>) -> ()) {

        sendPacket(packet: .getAccountId(id: id)) { result in

            switch result {

                case .success(let response): completion( .success( response ) )

                case .failure(let error): completion( .failure( .GetTransparentAccountIdError(message: "\(error)") ) )
            }
        }
    }

    static func getTransactions(id: String, page: Int, size: Int, completion: @escaping (Result<TransactionArray, SporkaError>) -> ()) {

        sendPacket(packet: .getTransactions(id: id, page: page, size: size)) { result in

            switch result {

                case .success(let response):

                    if let transactions = response["transactions"] as? Array<Dictionary<String, AnyObject>> {

                        let transformed = transactions.map { Transaction($0) }

                        completion( .success( transformed ) )
                    } else {
                        completion( .failure( .UnknownPacketFormat(message: "\(#function)") ) )
                    }

                case .failure(let error): completion( .failure( .GetTransactionsError(message: "\(error)") ) )
            }
        }
    }
}
