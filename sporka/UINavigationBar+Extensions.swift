//
//  UINavigationBar+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

extension UINavigationBar {

    static let navbarSize: CGFloat = 50
    static let navbarPadding: CGFloat = 10

    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: self.frame.size.width, height: UINavigationBar.navbarSize)
    }

    override open func willMove(toWindow newWindow: UIWindow?) {

        super.willMove(toWindow: newWindow)

        if let width = (UIApplication.shared.delegate as! AppDelegate).window?.bounds.size.width {

            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 1
            self.layer.shadowOffset = CGSize(width: 0, height: 2)

            let shadowOriginX = self.layer.bounds.origin.x - UINavigationBar.navbarPadding
            let shadowOriginY = self.layer.bounds.size.height - 6
            let shadowWidth = width + (2 * UINavigationBar.navbarPadding)

            let shadowPath = CGRect(x: shadowOriginX, y: shadowOriginY, width: shadowWidth, height: 5)
            self.layer.shadowPath = UIBezierPath(rect: shadowPath).cgPath
            self.layer.shouldRasterize = true
        }
    }

    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        self.willMove(toWindow: (UIApplication.shared.delegate as! AppDelegate).window)
    }
}
