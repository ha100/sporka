//
//  ArrayDataSource.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class ArrayDataSource<T>: NSObject, Sequence, UITableViewDataSource {

    lazy var originalData: Array<T> = {
        return Array<T>()
    }()

    var cellIdentifier: String

    init(identifier: String) {
        self.cellIdentifier = identifier
    }

    func itemAtIndexPath(indexPath: IndexPath ) -> T {
        return self[indexPath.row]
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let background: UIColor = indexPath.row % 2 == 0 ? UIColor.sporkaLightGray : .white

        switch T.self {

            case is Account.Type:

                let cell: AccountCell = tableView.dequeueReusableCell(for: indexPath)
                let item = self.itemAtIndexPath(indexPath: indexPath)
                cell.configureCellForObject(item: item)
                cell.backgroundColor = background
                return cell

            case is Transaction.Type:

                let cell: TransactionCell = tableView.dequeueReusableCell(for: indexPath)
                let item = self.itemAtIndexPath(indexPath: indexPath)
                cell.configureCellForObject(item: item)
                cell.backgroundColor = background
                return cell

            default: fatalError(">>> case not implemented")
        }

        return UITableViewCell()
    }
}
