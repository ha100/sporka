//
//  Result+Extensions.swift
//  sporka
//
//  Created by tom Hastik on 08/04/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

extension Result {

    func map<U>( function: (T) -> U ) -> Result<U, E> {
        return flatMap { .success( function( $0 ) ) }
    }

    func flatMap<U>( function: (T) -> Result<U, E> ) -> Result<U, E> {

        switch self {

            case .success(let value): return function(value)

            case .failure(let error): return .failure(error)
        }
    }
}
