//
//  TypeAliases.swift
//  sporka
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

typealias AccountArray = Array<Account>

typealias AccountDataSource = ArrayDataSource<Account>

typealias AttributeDictionary = Dictionary<NSAttributedString.Key, NSObject>

typealias Index = Int

typealias JSON = Dictionary<String, AnyObject>

typealias TransactionArray = Array<Transaction>

typealias TransactionDataSource = ArrayDataSource<Transaction>
