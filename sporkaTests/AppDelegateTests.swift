//
//  sporkaTests.swift
//  sporkaTests
//
//  Created by tom Hastik on 28/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import XCTest
@testable import sporka

class sporkaTests: XCTestCase {
    
    var sut: AppDelegate!
    
    override func setUp() {

        super.setUp()
        sut = AppDelegate()
    }
    
    override func tearDown() {

        sut = nil
        super.tearDown()
    }
    
    func testDidFinishLaunchingWithOptionsReturnsTrue() {
        XCTAssertTrue(sut.application(UIApplication.shared, didFinishLaunchingWithOptions: nil), "ApplicationDidFinishLaunching should return True")
    }
    
    func testWindowExists() {
        sut.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        XCTAssertNotNil(sut.window, "Window should be created at app start")
    }
    
    func testRootControllerExists() {
        sut.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        XCTAssertNotNil(sut.window!.rootViewController, "RootViewcontroller should be created and set");
    }
    
    func testRootViewControllerValidates() {
        sut.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        XCTAssertTrue(sut.window!.rootViewController is UINavigationController, "RootViewController should be NavigationController");
    }
}
