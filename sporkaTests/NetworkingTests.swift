//
//  NetworkingTests.swift
//  sporkaTests
//
//  Created by tom Hastik on 29/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import XCTest
@testable import sporka

class NetworkingTests: XCTestCase {

    class MockNetworking: Networking {}

    var sut: MockNetworking!
    var request: NSURLRequest!

    override func setUp() {
        super.setUp()
        sut = MockNetworking()

        do {
            request = try sut.generateRequest(.GetTransparentAccounts)
        } catch {
            print("FAIL")
        }
    }

    override func tearDown() {
        sut = nil
        request = nil
        super.tearDown()
    }

    func testRequestContainsValidGzipHeaders() {
        XCTAssertTrue(request.allHTTPHeaderFields!["Accept-Encoding"] == "gzip", "Request should be gzipped")
    }

    func testRequestContainsValidLanguageHeaders() {
        XCTAssertTrue(request.allHTTPHeaderFields!["Accept-Language"] == "cs-CZ", "Request should contain valid language reference")
    }

    func testRequestContainsValidAcceptHeaders() {
        XCTAssertTrue(request.allHTTPHeaderFields!["Accept"] == "application/json", "Request should contain valid value for Accept Key")
    }

    func testRequestContainsValidContentTypeHeaders() {
        XCTAssertTrue(request.allHTTPHeaderFields!["Content-Type"] == "application/json; charset=UTF-8", "Request should contain valid value for Content-Type key")
    }

    func testRequestContainsApiKeyHeaders() {
        XCTAssertNotNil(request.allHTTPHeaderFields!["WEB-API-key"], "Request should contain value for WEB-API key")
    }

    func testRequestApiKeyIsNotDefaultValueHeaders() {
        XCTAssertFalse(sut.safelyRetrieveApiKey() == "N/A", "Request should contain value for WEB-API key but N/A is present. is Sensitive.plist present in bundle as requested?")
    }
}
