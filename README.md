# sporka
sample project to demonstrate access to banking api

in order to successfully compile the project, create the Sensitive.plist file in your bundle directory, with Key "WEB-API-key" and appropriate Value obtained from [Erste Developer Portal](https://developers.erstegroup.com)

## register as Erste developer

|  |  |
| --- | --- |
| <img width="400" alt="erste-developer-portal" src="https://user-images.githubusercontent.com/2852881/49119125-5c7dab00-f2a7-11e8-9bc3-85c8d1aed91e.png"> | 1) go to [Erste Developer Portal](https://developers.erstegroup.com) and sign up for a new developer account by clicking on the sign up button located in the upper right corner |
| <img width="400" alt="erste-developer-portal-sign-up" src="https://user-images.githubusercontent.com/2852881/49119150-70291180-f2a7-11e8-9572-8dec4a499196.png"> | 2) fill in the required info |
| <img width="400" alt="erste-developer-portal-create-new-app" src="https://user-images.githubusercontent.com/2852881/49119151-74552f00-f2a7-11e8-89ce-087f4cf70bbe.png"> | 3) click on the "Create App" tile |
| <img width="400" alt="erste-developer-portal-create-app" src="https://user-images.githubusercontent.com/2852881/49119175-8df67680-f2a7-11e8-940c-674f0cd332ba.png"> | 4) fill in the app details |
| <img width="400" alt="erste-developer-portal-connect-bank-api" src="https://user-images.githubusercontent.com/2852881/49119308-1bd26180-f2a8-11e8-8541-d693cb802b7f.png"> | 5) connect the Ceska Sporitelna Transparent Account API to your app |
| <img width="400" alt="erste-developer-portal-bank-detail" src="https://user-images.githubusercontent.com/2852881/49119398-7c619e80-f2a8-11e8-90d8-0a59307f06f0.png"> | 6) select the app settings |
| <img width="400" alt="erste-developer-portal-api-key" src="https://user-images.githubusercontent.com/2852881/49119316-23920600-f2a8-11e8-88a8-0a4e7d78e5b1.png"> | 7) scroll down to get the WEB-API-key |

## create Sensitive.plist

|  |  |
| --- | --- |
| <img width="400" alt="xcode-create-plist" src="https://user-images.githubusercontent.com/2852881/49120192-ffd0bf00-f2ab-11e8-89c6-711e11bc31f6.png"> | 1) add new plist file to the project, name it Sensitive.plist and add a key "WEB-API-key" with a String value containing your API key obtained in the previous step |

## build & run

## todo

pagination
filtering

